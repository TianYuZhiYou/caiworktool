

class Config(object):
    "为app配置参数"
    DEBUG = True

    # this is the rabbitmq's host and port
    HOST = '192.168.1.22'
    PORT = 5672

    # set the rabbitmq's queue,exchange and rout_key,you can add to more.
    QUEUENAME = 'eeg'
    EXCHANGE = 'bbb'
    ROUT_KEY = 'eeg'

    # Mayble you need to set the connent your data.
    FILENAME = './260k.txt'

    # set the username and password that you connect to the rabbitmq.
    USERNAME = 'passwd'
    PASSWD = 'passwd'