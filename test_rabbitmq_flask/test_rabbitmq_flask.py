from flask import Flask,current_app,request
from setting import *
import logging
from pool_sender_open import MQClient
import os
import json

app = Flask(__name__)


# 引入配置文件
app.config.from_object(Config)

# 初始化对象，创建信道池
client = MQClient()
print(os.getpid())
client.create_pool_channel(10)

@app.route('/test',methods=['POST','GET'])
def test():
    # data= request.args.get('data')
    dict1 = request.get_json()
    data = dict1.get('data',None)
    # print(data)
    print(type(data))
    print(os.getpid())
    if data is not None:
        client.publish_message(data)
    return 'Hello World!'


if __name__ == '__main__':
    app.run(host="192.168.1.108",port=8000,debug=True)
