# #!/usr/bin/env python

"""
如果远程客户端强迫关闭了连接，所有的信道将会失效，通过尝试推送消息捕获异常，如果不能成功，将
删除双向队列里的所有的信道对象，重新创建新的连接池。

"""

import pika
from setting import *
from collections import deque
import time

class MQClient(object):
    def __init__(self):
        self.__host = Config.HOST
        self.__port = Config.PORT
        self.__queuename = Config.QUEUENAME
        self.__exchangename = Config.EXCHANGE
        self.__rout = Config.ROUT_KEY
        self.__username = Config.USERNAME
        self.__passwd = Config.PASSWD
        # self.connection = self._connect_rabbitmq()
        self.pool = deque(maxlen=1000)

    # 连接mq队列
    def _connect_rabbitmq(self):
        # 添加用户名和密码
        credentials = pika.PlainCredentials(self.__username, self.__passwd)
        # 配置连接参数
        parameters = pika.ConnectionParameters(host=self.__host,
                                               port=self.__port,
                                               credentials=credentials)
        # 创建一个连接对象
        connection = pika.BlockingConnection(parameters)
        return connection

    # 创建单个信道
    def create_one_channel(self):
        connection = self._connect_rabbitmq()
        channel = connection.channel()
        return channel

    # 创建信道池
    def create_pool_channel(self,nummber):
        connection = self._connect_rabbitmq()
        for i in range(nummber):
            channel = connection.channel()
            self.pool.append(channel)

    # 设置信道的配置
    def set_channel(self,channel,confirm=False):
        # 开启确认模式
        if confirm:
            channel.confirm_delivery()

    # 打开数据文件
    def open_data(self,filename):
        try:
            with open(filename,'r',encoding='utf-8') as f:
                data = f.read()
                return data
        except Exception as e:
            print(e)

    # 推送消息
    def channel_publish(self,channel,body):
        for i in range(3):
            channel.queue_declare(queue=self.__queuename, durable=True)
            result = channel.basic_publish(exchange=self.__exchangename,
                                           routing_key=self.__rout,
                                           body=body)
            if result:
                break

    # 推送消息,实现负载均衡
    def publish_message(self,body):
        for i in range(5):
            try:
                channel = self.pool.pop()
            except IndexError:
                self.create_pool_channel(20)
                channel = self.pool.pop()
            print(i)
            print(body)
            try:
                self.channel_publish(channel,body)
            except Exception as e:
                print(e)
                self.pool.clear()
            else:
                self.pool.appendleft(channel)
                break

    # 关闭连接
    def close_connect(self,connection):
        connection.close()

if __name__ == '__main__':
    # 测试用例
    time1 = time.time()
    client = MQClient()
    client.create_pool_channel(10)
    message = client.open_data(Config.FILENAME)
    num = 100
    for i in range(num):
        print(i)
        client.publish_message(message)
    time2 = time.time()
    print(num / (time2 - time1))

