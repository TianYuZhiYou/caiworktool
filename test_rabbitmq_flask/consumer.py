import requests

def test_request_file(num):
    url = 'http://192.168.1.108:8000/test'
    filename = './260k.txt'
    file = open(filename, 'r',encoding='utf-8')
    # print(file.read())
    files = {"file": file}
    i = 0
    while i < num:
        try:
            print(i)
            res = requests.post(url, files=files)
        except Exception as e:
            print(e)
        else:
            i += 1
    file.close()

def test_request_json(num):
    url = 'http://192.168.1.108:8000/test'
    filename = './260k.txt'
    file = open(filename, 'r', encoding='utf-8')
    data = {'data':file.read()}
    i = 0
    while i < num:
        try:
            print(i)
            res = requests.post(url, json=data)
        except Exception as e:
            print(e)
        else:
            i += 1

if __name__ == '__main__':
    # test_request_file(2)
    test_request_json(2)