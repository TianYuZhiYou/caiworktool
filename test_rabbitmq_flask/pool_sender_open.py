# #!/usr/bin/env python

"""
如果远程客户端强迫关闭了连接，所有的信道将会失效，通过直接判断当前的连接是否还是打开的，
然后来对信道池进行操作。

"""

import pika
from setting import *
from collections import deque
import time

class MQClient(object):
    def __init__(self):
        # 初始化对象，加载配置文件
        self.__host = Config.HOST
        self.__port = Config.PORT
        self.__queuename = Config.QUEUENAME
        self.__exchangename = Config.EXCHANGE
        self.__rout = Config.ROUT_KEY
        self.__username = Config.USERNAME
        self.__passwd = Config.PASSWD
        self.connection = self._connect_rabbitmq()
        self.pool = deque(maxlen=1000)

    # 连接mq队列
    def _connect_rabbitmq(self):
        # 添加用户名和密码
        credentials = pika.PlainCredentials(self.__username, self.__passwd)
        # 配置连接参数
        parameters = pika.ConnectionParameters(host=self.__host,
                                               port=self.__port,
                                               credentials=credentials)
        # 创建一个连接对象
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        return connection

    # 创建单个信道
    def create_one_channel(self):
        channel = self.connection.channel()
        return channel

    # 创建信道池
    def create_pool_channel(self,nummber):
        for i in range(nummber):
            channel = self.connection.channel()
            self.pool.append(channel)

    # 设置信道的配置
    def set_channel(self,channel,confirm=False):
        # 开启确认模式
        if confirm:
            channel.confirm_delivery()

    # 打开数据文件
    def open_data(self,filename):
        try:
            with open(filename,'r',encoding='utf-8') as f:
                data = f.read()
                return data
        except Exception as e:
            print(e)

    # 推送消息，三次推送确保成功
    def channel_publish(self,channel,body):
        for i in range(3):
            # 声明一个队列，持久化
            channel.queue_declare(queue=self.__queuename, durable=True)
            result = channel.basic_publish(exchange=self.__exchangename,
                                           routing_key=self.__rout,
                                           body=body)
            if result:
                break

    # 重新初始化连接对象
    def reset_connection(self):
        # 先清空原来的信道池
        self.pool.clear()
        # 创建新的TCP连接对象
        self.connection = self._connect_rabbitmq()
        # 创建新的信道加入双向队列
        self.create_pool_channel(20)

    # 推送消息,实现负载均衡
    def publish_message(self,body):
        print(body)
        # 判断如果连接是关闭的，清空信道池，重新创建连接
        if self.connection.is_closed:
            self.reset_connection()
        try:
            # 弹出一个可用的信道来使用
            channel = self.pool.pop()
            # 如果该信道是不可用的，重置连接
            if channel.is_closed:
                self.reset_connection()
                channel = self.pool.pop()
        except IndexError:
            # 如果队列的对象空了,添加更多的信道对象进入双向队列
            # 如果TCP连接是打开的
            if self.connection.is_open:
                # 加入10个信道
                self.create_pool_channel(10)
            else:
                # 如果TCP连接失效了，重新初始化连接
                self.reset_connection()
            channel = self.pool.pop()

        try:
            # 尝试推送，如果发生错误重置连接对象，丢失数据
            self.channel_publish(channel, body)
        except Exception as e:
            print(e)
        finally:
            # 如果信道的连接对象没有关闭
            if channel.connection.is_open:
                # 如果没有报错重新将信道加入双向队列
                self.pool.appendleft(channel)

    # 关闭连接
    def close_connect(self,connection):
        connection.close()

if __name__ == '__main__':
    # 测试用例
    time1 = time.time()
    client = MQClient()
    client.create_pool_channel(10)
    message = client.open_data(Config.FILENAME)
    num = 100
    for i in range(num):
        print(i)
        client.publish_message(message)
    time2 = time.time()
    print(num / (time2 - time1))

