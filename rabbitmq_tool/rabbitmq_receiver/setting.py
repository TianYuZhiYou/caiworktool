
"""消费者的配置文件"""

# this is the rabbitmq's host and port
HOST = '192.168.1.22'
PORT = 5672

# set the rabbitmq's queue,exchange and rout_key,you can add to more.
QUEUENAME = 'eeg'
EXCHANGE = 'bbb'
ROUT_KEY = 'eeg'
EXCHANGE_TYPE = 'direct'
# set the username and password that you connect to the rabbitmq.
USERNAME = 'passwd'
PASSWD = 'passwd'
