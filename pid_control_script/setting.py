"""
脚本的配置文件
"""

# 日志文件存储路径
PATHLOG = './var/log.txt'
# 脚本进程文件路径
PATHPID = './var/pid.txt'
# 监控进程号保存路劲
CPATHPID = './var/cpid.txt'
# 设置输出格式
FORMAT = '%(asctime)s %(levelname)s: %(message)s'
# 设置日志的记录级别
LEVEL = 20

# 目标程序的启动指令
CMD = 'python3 test_script.py'
# 目标程序监控脚本指令
CCMD = 'python3 control_test.py'

# 设置监听脚本监听频率，时间单位为s
TIME = 60*5