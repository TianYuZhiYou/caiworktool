'''
设置日志对象文件
'''

import logging
import sys
from setting import *

def get_logger(appname:str=''):
    '''
    配置一个日志对象，日志同时输出到文件和控制台
    :param appname:对象的名字
    :return: 日志对象
    '''
    # 获取logger实例，如果参数为空则返回root logger
    logger = logging.getLogger(appname)
    # 指定logger输出格式
    formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s')
    # 指定文件日志
    file_handler = logging.FileHandler(PATHLOG)
    file_handler.setFormatter(formatter)
    # 控制台日志
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.formatter = formatter  # 也可以直接给formatter赋值
    # 为logger添加的日志处理器，可以自定义日志处理器让其输出到其他地方
    logger.addHandler(file_handler)
    logger.addHandler(console_handler)
    # 指定日志的最低输出级别，默认为WARN级别
    logger.setLevel(LEVEL)
    return logger

def get_logger_file(appname:str=''):
    '''
    日志输出到文件。
    :param appname:
    :return: 日志对象
    '''
    # 获取logger实例，如果参数为空则返回root logger
    logger = logging.getLogger(appname)
    # 指定logger输出格式
    formatter = logging.Formatter(FORMAT)
    # 指定文件日志
    file_handler = logging.FileHandler(PATHLOG)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    # 指定日志的最低输出级别，默认为WARN级别
    logger.setLevel(logging.INFO)
    return logger