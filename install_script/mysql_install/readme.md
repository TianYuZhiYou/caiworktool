## 安装mysql

- 使用root权限

- 先手动下载mysql版本，解压；

- 执行命令：

```
$ python3 install_mysql.py mysqlname(绝对路径)
```

- 详细参考：[http://www.cnblogs.com/cwp-bg/p/8311528.html](http://www.cnblogs.com/cwp-bg/p/8311528.html)

## 卸载mysql

- 使用root权限

- 执行命令：

```
$ python3 uninstall_mysql.py
```