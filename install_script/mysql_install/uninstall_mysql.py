#coding=utf-8

"""
卸载mysql
"""
import os
import sys

def uninstall_mysql():
    try:
        os.system('rm -rf *mysql*')
        os.system('rm -rf /etc/init.d/mysql')
    except Exception as e:
        print(e)

if __name__ == '__main__':
    uninstall_mysql()
