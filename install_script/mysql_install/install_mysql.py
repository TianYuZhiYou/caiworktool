#coding=utf-8

"""
linux系统安装mysql数据库

# 将下载包解压到/usr/local/目录下
tar mysql-advanced-5.7.20-linux-glibc2.12-x86_64 -C /usr/local
# 创建一个mysql的用户组
groupadd mysql
# 为用户组添加用户
useradd -r -g mysql mysql
cd /usr/local
# 为解压文件创建一个简单的软链接
ln -s mysql-advanced-5.7.20-linux-glibc2.12-x86_64 mysql
cd mysql
# 创建一个用户数据的文件
mkdir mysql-files
# 将该文件夹交给mysql组下的mysql用户
chown mysql:mysql mysql-files
# 授权
chmod 750 mysql-files
# 执行初始化脚本
cd bin
./mysqld --initialize --user=mysql  # ./mysql_intsall_db脚本初始化方式已经被弃用
# 添加加密
./mysql_ssl_rsa_setup
# 安全启动mysql
./mysqld_safe --user=mysql&

# 将mysql自带的启动脚本添加到init.d下
cp /usr/local/mysql/support-files/mysql.server /etc/init.d/mysql
# 注册到自启动服务
chkconfig --add mysql
chkconfig mysql on

https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-5.7.21-el7-x86_64.tar.gz
"""

import os
import sys

def install_mysql(MYSQL):
    try:
        mysql_name = MYSQL.split('/')[-1]
        # 测试是否安装yum或apt
        try:
            os.system('yum version')
        except:
            os.system('apt list --upgradable')
            # 安装相关的依赖包
            os.system('apt install libaio')
        else:
            # 安装相关的依赖包
            os.system('yum -y install libaio')
        print("相关依赖环境安装完毕！")
        print("\n---\n---\n---\n---\n---")
        # 移动
        os.system('mv %s /usr/local/'%MYSQL)
        # 创建软链接
        os.system('ln -s /usr/local/%s /usr/local/mysql'%mysql_name)
        print("创建用户组。。。")
        os.system('groupadd mysql && useradd -r -g mysql mysql')
        print("创建用户数据文件。。。")
        os.system('cd /usr/local/mysql/ && mkdir mysql-files && chown mysql:mysql mysql-files')
        os.system('chmod 750 /usr/local/mysql/mysql-files')
        print("执行初始化脚本。。。")
        os.system('/usr/local/mysql/bin/mysqld --initialize --user=mysql')
        print("加密。。。")
        os.system('/usr/local/mysql/bin/mysql_ssl_rsa_setup')
        print("启动mysql。。。")
        os.system('/usr/local/mysql/bin/mysqld_safe --user=mysql&')
        print("安装完毕!")
        print("\n---\n---\n---\n---\n---")
        print("设置开机自启动。。。")
        os.system('cp /usr/local/mysql/support-files/mysql.server /etc/init.d/mysql')
        os.system('chkconfig --add mysql && chkconfig mysql on')
        os.system('ln -s /usr/local/mysql/bin/mysql /usr/bin/mysql')
    except Exception as e:
        print(e)

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("有且需要一个参数！")
    else:
        MYSQL= sys.argv[1]
        install_mysql(MYSQL)

