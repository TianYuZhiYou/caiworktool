#coding=utf-8

'''
rabbitmq的安装脚本

# 从官网下载,在任何目录下,使用root权限
wget http://www.erlang.org/download/otp_src_20.0.tar.gz
# 解压到习惯目录
tar -zxvf otp_src_20.0.tar.gz -C /usr/local/
# 修改名字
mv otp_src_20.0 erlang
# 安装二郎需要的依赖包
yum -y install gcc glibc-devel make ncurses-devel openssl openssl-devel autoconf gcc-c++ kernel-devel m4  unixODBC unixODBC-devel wxGTK wxGTK-devel fop libxslt libxslt-devel
# 进入目录
cd erlang
# 指定安装的目录和相关的配置
./configure --prefix=/usr/local/erlang --enable-gui=no --enable-threads --enable-smp-support --enable-kernel-poll --enable-native-libs --enable-hipe --without-javac
# 如果没有报错，或只出现没有  wx not found和Java compiler disabled by user是正常的
make & make install

cd /usr/local
wget http://www.rabbitmq.com/releases/rabbitmq-server/v3.6.10/rabbitmq-server-generic-unix-3.6.10.tar.xz
# 解压
xz -d rabbitmq-server-generic-unix-3.6.10.tar.xz
tar -xvf rabbitmq-server-generic-unix-3.6.10.tar

# 重新命名
mv rabbitmq_server-3.6.10 rabbitmq_server
# 进入文件目录，这已经算是安装好的文件了

'''

import os
import subprocess
from subprocess import PIPE

# erlang版本设置
ERL_VERSION = '20.0'
# rabbitmq版本设置
RAB_VERSION = '3.6.10'
# 本地IP地址
IP = '192.168.1.21'

# 安装erlang语言环境
def install_erlang():
    try:
        p = subprocess.Popen('yum version',shell=True,stdout=PIPE)
        if 'not found' in p.stdout.read():
            print('pass')

        # 测试是否安装yum或apt
        try:
            os.system('yum version')
        except:
            os.system('apt list --upgradable')
            # 安装相关的依赖包
            os.system('apt install gcc glibc-devel make ncurses-devel \
                        openssl openssl-devel autoconf gcc-c++ kernel-devel m4 unixODBC \
                        unixODBC-devel wxGTK wxGTK-devel fop libxslt libxslt-devel')
        else:
            # 安装相关的依赖包
            os.system('yum -y install gcc glibc-devel make ncurses-devel \
            openssl openssl-devel autoconf gcc-c++ kernel-devel m4 unixODBC \
            unixODBC-devel wxGTK wxGTK-devel fop libxslt libxslt-devel')

        print("相关依赖环境安装完毕！")
        print("\n---\n---\n---\n---\n---")
        # 切换目录下载erlang
        print("开始下载erlang")
        os.system('cd /usr/local/ && wget http://www.erlang.org/download/otp_src_%s.tar.gz'%ERL_VERSION)
        # 解压
        os.system('tar -zxvf /usr/local/otp_src_%s.tar.gz -C /usr/local/'%ERL_VERSION)
        # 删除下载包
        os.remove('/usr/local/otp_src_%s.tar.gz'%ERL_VERSION)
        # 修改名字
        os.rename('/usr/local/otp_src_%s'%ERL_VERSION,'/usr/local/erlang')
        # 配置
        print("生成配置文件.....")
        os.system('cd /usr/local/erlang/ && /usr/local/erlang/configure --prefix=/usr/local/erlang --enable-gui=no \
        --enable-threads --enable-smp-support --enable-kernel-poll --enable-native-libs \
        --enable-hipe --without-javac')
        # 编译安装
        print("编译安装。。。。。。")
        os.system('cd /usr/local/erlang/ && make && make install')
        print("安装完毕!")

    except Exception as e:
        print(e)

    else:
        # 创建软链接
        print("创建软链接。。。")
        os.system('ln -s /usr/local/erlang/bin/erl /usr/bin/erl')
        print("完毕！")
        print("\n---\n---\n---\n---\n---")

# 安装rabbitmq
def install_rabbitmq():
    try:
        # 切换目录
        # 下载rabbitmq
        print("开始下载rabbitmq。。。")
        os.system('cd /usr/local/ && wget http://www.rabbitmq.com/releases/rabbitmq-server/v%s/rabbitmq-server-generic-unix-%s.tar.xz'%(RAB_VERSION,RAB_VERSION))
        # 解压
        os.system('xz -d /usr/local/rabbitmq-server-generic-unix-%s.tar.xz'%RAB_VERSION)
        os.system('tar -xvf /usr/local/rabbitmq-server-generic-unix-%s.tar -C /usr/local/'%RAB_VERSION)
        # 重命名
        os.rename('/usr/local/rabbitmq_server-%s'%RAB_VERSION,'/usr/local/rabbitmq')
        # os.remove('/usr/local/rabbitmq-server-generic-unix-%s.tar'%RAB_VERSION)
        # 添加环境变量
        # 先复制一份
        print("配置环境变量。。。")
        os.system('cp -f /etc/profile /etc/profile1')
        with open('/etc/profile','a',encoding='utf-8') as f:
            f.write('\n# set rabbitmq\nexport PATH=$PATH:/usr/local/rabbitmq/sbin')
        # 添加主机名
        print("添加主机名。。。")
        name = new_hostname()
        os.system('cp -f /etc/hosts /etc/hosts1')
        with open('/etc/hosts','a',encoding='utf-8') as f:
            # 判断是否存在
            con_list = f.readlines()
            cmd1 = '127.0.0.1   %s'%name
            cmd2 = '%s   %s' % (IP,name)
            if cmd1 not in con_list:
                f.write('\n' + cmd1)
            if cmd2 not in con_list:
                f.write('\n' + cmd2)

    except Exception as e:
        print(e)
    else:
        print('安装完毕！请手动重启服务器测试！')

# 获取系统主机名
def hostname():
    sys = os.name
    if sys == 'nt':
        hostname = os.getenv('computername')
        return hostname
    elif sys == 'posix':
        host = os.popen('echo $HOSTNAME')
        try:
            hostname = host.read()
            return hostname
        finally:
            host.close()
    else:
        return 'Unkwon hostname'

def new_hostname():
    name = hostname()
    list1 = name.split('.')
    if len(list1) > 1:
        name = list1[0]
    return name

if __name__ == '__main__':
    # install_erlang()
    # install_rabbitmq()

    p = subprocess.Popen('yum version', shell=True, stdout=PIPE)
    if 'not found' in p.stdout.read().decode('utf-8'):
        print('pass')
