
## 安装rabbitmq

- 使用root权限执行脚本；

- 在终端窗口执行命令：

```
$ python3 rabbitmq_install_script.py
```

## 卸载rabbitmq

- 执行命令：

```
$ python3 rabbitmq_uninstall.py
```

- 详细内容参考

- [http://www.cnblogs.com/cwp-bg/p/8397529.html](http://www.cnblogs.com/cwp-bg/p/8397529.html)