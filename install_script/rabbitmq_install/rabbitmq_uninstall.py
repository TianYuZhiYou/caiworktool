
#coding=utf-8
"""
卸载rabbitmq和erlang语言环境
"""

import os

def uninstall_erl():
    """卸载erlang环境，删除相关的文件"""
    try:
        os.system('rm -rf /usr/local/erlang')
        print("删除安装文件成功！")
        # 删除软连接
        os.system('rm -rf /usr/bin/erl')
        print("删除链接成功！")
    except Exception as e:
        print(e)


def uninstall_rab():
    """卸载rabbitmq环境，删除相关的文件"""
    try:
        os.system('rm -rf /usr/local/rabbitmq')
        print('已删除安装文件！')
        # 删除环境变量
        os.system('rm -f /etc/profile')
        os.rename('/etc/profile1','/etc/profile')
        print("删除环境变量成功！")
    except Exception as e:
        print(e)


if __name__ == '__main__':
    uninstall_erl()
    uninstall_rab()
