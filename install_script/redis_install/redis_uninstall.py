#coding=utf-8

"""
删除redis文件

"""
import os
import time

REDIS_PORT = '6379'

def uninstall_redis():
    try:
        # 删除相关的python安装文件
        print("开始删除安装文件。。。")
        os.system('rm -rf /usr/local/redis')
        print("删除相关配置文件。。。")
        try:
            os.system('rm -rf /etc/redis/*')
        except:
            pass
        try:
            os.system('rm -rf /var/log/redis*')
        except:
            pass
        try:
            os.system('rm -rf /var/lib/redis/*')
        except:
            pass
        try:
            os.system('rm -rf /usr/local/bin/redis*')
        except:
            pass
        try:
            os.system('rm -rf /etc/init.d/redis*')
        except:
            pass
        print("卸载完毕！准备重启服务器。。。")
        time.sleep(3)
        os.system('reboot')

    except Exception as e:
        print(e)

if __name__ == '__main__':
    uninstall_redis()