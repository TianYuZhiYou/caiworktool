#coding=utf-8

"""
redis缓存系统的安装脚本

# 从官方网站下载安装包，注意，当前在哪个目录下执行命令，下载的包将在哪个目录下
$ wget http://download.redis.io/releases/redis-4.0.6.tar.gz
# 将下载包解压
$ tar -zxvf redis-4.0.6.tar.gz
# 进入解压后的文件夹
$ cd redis-4.0.6
# 对文件进行编译，得到可执行的文件，
$ sudo make
# 对编译的文件进行测试,时间较长，没有报错则没有问题，编译出的redis命令放在src目录下
$ sudo make test
# 一般将redis文件统一放在/usr/local目录下,因此将文件移动
$ sudo mv ./redis-4.0.6/* /usr/local/redis/
# 进入redis的目录，执行安装
$ cd /usr/local/redis/
$ sudo make install
# 生成配置文件
$ cd utils
$ ./install_server.sh
"""

import os
import sys

# 设置redis的版本
REDIS_VERSION = '4.0.6'

def install_redis():
    try:
        # 切换目录下载erlang
        print("开始下载redis")
        os.system('cd /usr/local/ && wget http://download.redis.io/releases/redis-%s.tar.gz'%REDIS_VERSION)
        # 解压
        print("解压。。。")
        print("\n-------\n")
        os.system('tar -zxvf /usr/local/redis-%s.tar.gz -C /usr/local/'%REDIS_VERSION)
        # 删除下载包
        os.remove('/usr/local/redis-%s.tar.gz'%REDIS_VERSION)
        # 修改名字
        os.rename('/usr/local/redis-%s'%REDIS_VERSION,'/usr/local/redis')
        # 编译安装
        print("编译测试安装。。。。。。")
        os.system('cd /usr/local/redis/ && make && make test && make install')
        print("安装完毕!")
        print("\n-------\n")
        print("修改启动配置文件。。。")
        os.system('/usr/local/redis/utils/install_server.sh')

    except Exception as e:
        print(e)
    else:
        print("安装redis完毕！请测试！")

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("默认安装redis-4.0.6。。。")
        install_redis()
    elif len(sys.argv) == 2:
        REDIS_VERSION = sys.argv[1]
        install_redis()
    elif len(sys.argv) > 2:
        print("最多只能输入一个参数！")