## 安装redis

- 请使用root权限

- 执行命令：

```
$ python3 redis_install.py
```

- 详细参考：

- [http://www.cnblogs.com/cwp-bg/p/8094914.html](http://www.cnblogs.com/cwp-bg/p/8094914.html)


## 卸载redis

- 请使用root权限

- 执行命令：

```
$ python3 redis_uninstall.py
```