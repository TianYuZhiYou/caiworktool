#coding=utf-8

"""
卸载python，删除相关的文件
"""

import os

def uninstall_python():
    try:
        # 删除相关的python安装文件
        os.system('rm -rf /usr/local/python*')
        # 删除python软连接
        os.system('rm -rf /usr/bin/python3')
        # 删除pip的软连接
        os.system('rm -rf /usr/sbin/pip')
    except Exception as e:
        print(e)

if __name__ == '__main__':
    uninstall_python()



