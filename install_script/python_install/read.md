## 安装python环境

- 使用root权限执行，linux环境中预装了Python2；

- 设置需要安装的Python版本；

```
vi install_python.py

# 修改PY_VERSION参数
```

- 执行命令

```
$ python2 install_python.py
```

## 详细说明文档

- [http://www.cnblogs.com/cwp-bg/p/8274379.html](http://www.cnblogs.com/cwp-bg/p/8274379.html)