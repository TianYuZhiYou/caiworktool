#!/usr/bin/python
#coding=utf-8

"""
安装python环境的脚本

# 删除原来的python连接
cd /usr/bin/
rm python
# 创建python3的软连接
ln -s /usr/local/python3/bin /usr/bin/python
# 输入python发现默认打开python3.6则成功
# 修改yum依赖
vi /usr/bin/yum
# 将第一行/usr/bin/python改为/usr/bin/python2

"""
import os
import sys

# 安装配置
PY_VERSION = '3.6.3'


# 安装python环境
# def install_python():
#     # 检测是否安装yum或apt，安装对应的依赖环境
#     try:
#         os.system('yum version')
#     except:
#         os.system('apt list --upgradable')
#         # 安装相关的依赖包
#         os.system('apt install zlib-devel bzip2-devel openssl-devel \
#         ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel \
#         db4-devel libpcap-devel xz-devel gcc')
#     else:
#         # 安装相关的依赖包
#         os.system('yum -y install zlib-devel bzip2-devel openssl-devel \
#         ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel \
#         db4-devel libpcap-devel xz-devel gcc')
#     try:
#         cmd1 = 'cd /usr/local/'
#         cmd2 = 'wget -P /usr/local https://www.python.org/ftp/python/%s/Python-%s.tgz' % (PY_VERSION,PY_VERSION)
#         cmd3 = 'tar -zxvf Python-%s.tgz -C /usr/local/' % PY_VERSION
#         cmd4 = 'cd /usr/local/python%s'%PY_VERSION
#         cmd5 = '/usr/local/python%s/configure --prefix=/usr/local/python%s'%(PY_VERSION,PY_VERSION)
#         cmd6 = 'make'
#         cmd7 = 'make install'
#         # 切换目录
#         # 下载python
#         # 解压
#         os.system(cmd1 + ' && '+ cmd2 + ' && ' + cmd3)
#         # 删除下载包
#         os.remove('/usr/local/Python-%s.tgz' % PY_VERSION)
#         # 修改名字
#         os.rename('/usr/local/Python-%s' % PY_VERSION, '/usr/local/python%s'%PY_VERSION)
#         # 编译安装
#         os.system(cmd4 + ' && '+ cmd5  + ' && ' + cmd6+ ' && ' + cmd7)
#
#         # 配置环境变量
#         # os.system('rm -f /usr/bin/python')
#         # 创建python3的软连接
#         os.system('ln -s /usr/local/python%s/bin/python3 /usr/bin/python3'%PY_VERSION)
#         # 创建pip的软连接
#         os.system('ln -s /usr/local/python%s/bin/pip3 /usr/sbin/pip'%PY_VERSION)
#     except Exception as e:
#         print(e)

def install_python():
    cmd1 = 'cd /usr/local/'
    cmd2 = 'wget -P /usr/local https://www.python.org/ftp/python/%s/Python-%s.tgz' % (PY_VERSION, PY_VERSION)
    cmd3 = 'tar -zxvf Python-%s.tgz -C /usr/local/' % PY_VERSION
    cmd4 = 'cd /usr/local/python%s' % PY_VERSION
    cmd5 = '/usr/local/python%s/configure --prefix=/usr/local/python%s' % (PY_VERSION, PY_VERSION)
    cmd6 = 'make'
    cmd7 = 'make install'
    try:
        # 安装相关的依赖包
        os.system('apt install zlib* bzip2-devel openssl* \
        ncurses* readline* tk* gdbm* \
        db4* libpcap* xz* gcc')
        # 切换目录
        # 下载python
        # 解压
        os.system(cmd1 + ' && ' + cmd2 + ' && ' + cmd3)
        # 删除下载包
        os.remove('/usr/local/Python-%s.tgz' % PY_VERSION)
        # 修改名字
        os.rename('/usr/local/Python-%s' % PY_VERSION, '/usr/local/python%s' % PY_VERSION)
        # 编译安装
        os.system(cmd4 + ' && ' + cmd5 + ' && ' + cmd6 + ' && ' + cmd7)

        # 创建python3的软连接
        os.system('ln -s /usr/local/python%s/bin/python3 /usr/bin/python3.6' % PY_VERSION)
        # 创建pip的软连接
        os.system('ln -s /usr/local/python%s/bin/pip3 /usr/sbin/pip3.6' % PY_VERSION)
    except Exception as e:
        print(e)

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("默认安装python3.6.3。。。,您可以使用python install_python.py xxx 来指定安装的python版本")
        install_python()
    elif len(sys.argv) == 2:
        PY_VERSION = sys.argv[1]
        install_python()
    elif len(sys.argv) > 2:
        print("最多只能输入一个参数！")

