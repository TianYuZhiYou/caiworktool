#!/usr/bin/python
#coding=utf-8

"""
安装python的虚拟环境
"""

import os
import sys

# 安装配置
PY_VERSION = '3.6.3'

def install_virtual():
    cmd1 = 'pip install virtualenv'
    cmd2 = 'pip install virtualenvwrapper'
    cmd3 = 'source /usr/local/python%s/bin/virtualenvwrapper.sh' % PY_VERSION

    try:
        os.system(cmd1 + ' && ' + cmd2 + ' && ' + cmd3)
    except Exception as e:
        print(e)

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("默认安装python3.6.3版本的虚拟环境")
        install_python()
    elif len(sys.argv) == 2:
        PY_VERSION = sys.argv[1]
        install_python()
    elif len(sys.argv) > 2:
        print("最多只能输入一个参数！")