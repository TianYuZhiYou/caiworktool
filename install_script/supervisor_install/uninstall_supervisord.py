#!/usr/bin/python
#coding=utf-8

"""
卸载python的supervisor管理工具
"""

import os
import sys


def uninstall_supervisor():
    cmd1 = 'pip2 uninstall supervisor'
    cmd2 = 'mkdir /etc/supervisor'
    cmd3 = "rm -rf /etc/init.d/Ssupervisord.service"

    try:
        os.system(cmd1 + ' && ' + cmd2 + ' && ' + cmd3)
    except Exception as e:
        print(e)
    else:
        print('成功卸载supervisor.......')

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("开始卸载supervisor工具....")
        uninstall_supervisor()
    else:
        print("不能带参数！")