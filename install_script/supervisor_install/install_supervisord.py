#!/usr/bin/python
#coding=utf-8

"""
安装python的supervisor管理工具
"""

import os
import sys


def install_supervisor():
    cmd1 = 'pip2 install supervisor'
    cmd2 = 'mkdir /etc/supervisor'
    cmd3 = "echo_supervisord_conf > /etc/supervisor/supervisord.conf"
    cmd6 = 'chkconfig --add Ssupervisord.service'

    # 设置开机自启动
    cmd4 = 'touch /etc/init.d/Ssupervisord.service'
    cmd5 = 'chmod 777 /etc/init.d/Ssupervisord.service'
    line1 = '#!/bin/sh\n'
    line2 = '# chkconfig: 2345 20 80\n\n'
    line3 = '/usr/local/bin/supervisord -c /etc/supervisor/supervisord.conf'

    try:
        os.system(cmd1 + ' && ' + cmd2 + ' && ' + cmd3)
        os.system(cmd4 + ' && ' + cmd5)
        with open('/etc/init.d/Ssupervisord.service','w',encoding='utf-8') as f:
            f.writelines([line1,line2,line3])
        os.system(cmd6)
    except Exception as e:
        print(e)

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("开始安装supervisor工具....")
        install_supervisor()
    else:
        print("不能带参数！")